from math import sqrt

def out_of_place(desk, solution):
	size = len(desk)		
	h = 0 		# number of misplaced tiles
	for i in range(size):
		for j in range(size):
			if desk[i][j] != 0 and desk[i][j] != solution[i][j]:
				h += 1
	return h


def manhattan(desk, solution):

	def check_solution(i, j):
		for q in range(size):
			for w in range(size):
				if desk[ i ][ j ] == solution[ q ][ w ]:
					return abs(i - q) + abs(j - w) 

	size = len(desk)
	h = 0
	for i in range(size):
		for j in range(size):
			
			if desk[ i ][ j ] != 0:
				h += check_solution(i, j)


	return h


def euclidean(desk, solution):

	def check_solution(i, j):
		for q in range(size):
			for w in range(size):
				if desk[ i ][ j ] == solution[ q ][ w ]:
					return sqrt(abs(i - q)**2 + abs(j - w)**2) 

	size = len(desk)
	h = 0
	for i in range(size):
		for j in range(size):
			
			if desk[ i ][ j ] != 0:
				h += check_solution(i, j)


	return h
	