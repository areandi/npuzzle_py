class Node:

	def __init__(self, desk, solution, g, parent):
		self.desk = desk
		self.solution = solution
		self.g = g
		self.h = self.get_h()
		self.f = self.g + self.h
		self.parent = parent

	def __str__(self):
		return 'Node.f: %d' % self.f 
	# def __str__(self):
	# 	print('=== Node: ===')
	# 	print(self.parent)
	# 	print(self.children)
	# 	print(self.data)
	# 	print('=== ***** ===')
	# 	return ''


	def get_h(self):
		size = len(self.desk)		
		h = 0 		# number of misplaced tiles
		for i in range(size):
			for j in range(size):
				if (self.desk[i][j] != 0):
					if (self.desk[i][j] != self.solution[i][j]):
						h += 1
		return h

	def compare(self, arr):
		size = len(self.desk)
		for i in range(size):
			for j in range(size):
				if (self.desk[i][j] != arr[i][j]):
					return False
		return True


	def is_solved(self):
		size = len(self.solution)
		for i in range(size):
			for j in range(size):
				if (self.desk[i][j] != self.solution[i][j]):
					return False
		return True
	

	def swap_and_return_copy(self, i1, j1, i2, j2):
		size = len(self.desk)
		desk = [[self.desk[j][i] for i in range(size)] for j in range(size)]
		desk[i1][j1], desk[i2][j2] = desk[i2][j2], desk[i1][j1]
		return desk
		

	def expand(self):
		res = []
		size = len(self.desk)
		for i in range(size):
			for j in range(size):
				if self.desk[i][j] == 0:
					#	move 'zero-tile' UP
					if i != 0:
						res.append(
							Node(
								self.swap_and_return_copy(i, j, i - 1, j),
								self.solution,
								self.g + 1,
								self
							)
						)

					#	RIGHT
					if j != size - 1:
						res.append(
							Node(
								self.swap_and_return_copy(i, j, i, j + 1),
								self.solution,
								self.g + 1,
								self
							)
						)

					#	DOWN
					if i != size - 1:
						res.append(
							Node(
								self.swap_and_return_copy(i, j, i + 1, j),
								self.solution,
								self.g + 1,
								self
							)
						)

					#	LEFT
					if j != 0:
						res.append(
							Node(
								self.swap_and_return_copy(i, j, i, j - 1),
								self.solution,
								self.g + 1,
								self
							)
						)

					return res
		return res