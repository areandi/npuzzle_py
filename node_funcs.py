
def get_h(desk, solution):
	size = len(desk)		
	h = 0 		# number of misplaced tiles
	for i in range(size):
		for j in range(size):
			if (desk[i][j] != 0):
				if (desk[i][j] != solution[i][j]):
					h += 1
	return h

def get_h2(desk, solution):
	size = len(desk)	
	h = 0 		# number of misplaced tiles
	for i in range(size):
		if (desk[i] != 0):
			if (desk[i] != solution[i]):
				h += 1
	return h


def compare(arr1, arr2):
	size = len(arr1)
	for i in range(size):
		for j in range(size):
			if (arr1[i][j] != arr2[i][j]):
				return False
	return True


def is_solved(desk, solution):
	size = len(solution)
	for i in range(size):
		for j in range(size):
			if (desk[i][j] != solution[i][j]):
				return False
	return True


def swap_and_return_copy(desk, i1, j1, i2, j2):
	size = len(desk)
	desk = [[desk[j][i] for i in range(size)] for j in range(size)]
	desk[i1][j1], desk[i2][j2] = desk[i2][j2], desk[i1][j1]
	return desk

def swap_and_return_copy2(desk, i1, j1, i2, j2):
	size = len(desk)
	leng = 3
	desk = [desk[i] for i in range(size)]
	desk[i1 * leng + j1], desk[i2 * leng + j2] = desk[i2 * leng + j2], desk[i1 * leng + j1]
	return tuple(desk)

	

def expand(g, desk, solution):
	res = []
	size = 3
	for i in range(size):
		for j in range(size):
			if desk[i][j] == 0:

				#	move 'zero-tile' UP
				if i != 0:
					res.append(swap_and_return_copy(desk, i, j, i - 1, j))

				#	RIGHT
				if j != size - 1:
					res.append(swap_and_return_copy(desk, i, j, i, j + 1))

				#	DOWN
				if i != size - 1:
					res.append(swap_and_return_copy(desk, i, j, i + 1, j))

				#	LEFT
				if j != 0:
					res.append(swap_and_return_copy(desk, i, j, i, j - 1))

				return res
	return res