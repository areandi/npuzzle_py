import sys
import heapq

from Node import Node
from test2 import Test2


class Test:

	def __init__(self, desk, g):
		self.desk = desk
		self.g = g

def swap_and_return_copy2(desk, i1, j1, i2, j2):
	size = len(desk)
	leng = 3
	desk = [desk[i] for i in range(size)]
	desk[i1 * leng + j1], desk[i2 * leng + j2] = desk[i2 * leng + j2], desk[i1 * leng + j1]
	return tuple(desk)


# def create_solution_desk(size: int):
# 	desk = [[ 0 for x in range(size)] for x in range(size)]
# 	c = 1
	
# 	while True:
		

# 	return desk




# def move_right(arr, pointers):
# 	print('-')
# 	for j in range(pointers['j'], size):
# 		if {'i': pointers['i'], 'j': j} not in closed_pointers:
# 			pointers['j'] = j
# 			closed_pointers.append(dict(pointers))
# 			print(arr[pointers['i']][j])


# def move_down(arr, pointers):
# 	print('-')
# 	for i in range(pointers['i'], size):
# 		if  {'i': i, 'j': pointers['j']} not in closed_pointers:
# 			pointers['i'] = i
# 			closed_pointers.append(dict(pointers))
# 			print(arr[i][pointers['j']])


# def move_left(arr, pointers):
# 	print('-')
# 	for j in range(size - 1, -1, -1):
# 		if  {'i': pointers['i'], 'j': j} not in closed_pointers:
# 			pointers['j'] = j
# 			closed_pointers.append(dict(pointers))
# 			print(arr[pointers['i']][j])


# def move_up(arr, pointers):
# 	print('-')
# 	for i in range(size - 1, -1, -1):
# 		if {'i': i, 'j': pointers['j']} not in closed_pointers:
# 			pointers['i'] = i
# 			closed_pointers.append(dict(pointers))
# 			print(arr[i][pointers['j']])


def create_solution_desk(size):
	desk = [[0 for x in range(size)] for x in range(size)]
	sequence = list(range(size**2 - 1, 0, -1))
	sequence.insert(0, 0)
	closed_pointers = []
	pointers = {'i': 0, 'j': 0}

	#	1 - right; 2 - down; 3 - left; 4 - up
	dir = 0
	while sequence:

		dir += 1
		if dir > 4:
			dir = 1	
		
		if dir == 1:
			for j in range(pointers['j'], size):
				if {'i': pointers['i'], 'j': j} not in closed_pointers:
					pointers['j'] = j
					closed_pointers.append(dict(pointers))
					desk[pointers['i']][j] = sequence.pop()

		elif dir == 2:
			for i in range(pointers['i'], size):
				if  {'i': i, 'j': pointers['j']} not in closed_pointers:
					pointers['i'] = i
					closed_pointers.append(dict(pointers))
					desk[i][pointers['j']] = sequence.pop()

		elif dir == 3:
			for j in range(size - 1, -1, -1):
				if  {'i': pointers['i'], 'j': j} not in closed_pointers:
					pointers['j'] = j
					closed_pointers.append(dict(pointers))
					desk[pointers['i']][j] = sequence.pop()

		elif dir == 4:
			for i in range(size - 1, -1, -1):
				if {'i': i, 'j': pointers['j']} not in closed_pointers:
					pointers['i'] = i
					closed_pointers.append(dict(pointers))
					desk[i][pointers['j']] = sequence.pop()
	return desk
