import heapq
import time
import random
import sys
from Node import Node
import node_funcs as nf
# ================| C L A S S E S |================





# ====================*******======================

# ==============| F U N C T I O N S |==============

def print_desk(desk):
	if not isinstance(desk, list):
		print('This is not a list!')
		return False
	print()
	for row in desk:
		for col in row:
			print(col, end=" ")
		print()
	print()


def create_solution_desk(size: int):
	desk = [[ 0 for x in range(size)] for x in range(size)]
	c = 1
	for i in range(size):
		for j in range(size):
			if i == size - 1 and j == size - 1:
				desk[i][j] = 0
			else:
				desk[i][j] = c
				c += 1
	return desk



def create_desk(size: int):
	candidates = list(range(0, size**2))
	desk = [[ 0 for x in range(size)] for x in range(size)]
	for i in range(size):
		for j in range(size):
			desk[i][j] = candidates.pop(random.randrange(0, len(candidates)))
	return desk



def select_from_opened(opened):
	if opened:
		curr = opened[0]
	else:
		return None

	size = len(opened)
	for i in range(size):
		if curr.f > opened[i].f:
			curr = opened[i]
	return curr



def is_solvable(desk):
	size = len(desk)
	inv_count = 0
	arr = sum(desk, [])

	for i in range(len(arr)):
		for j in range(i + 1, len(arr)):
			if (arr[i] != 0 and arr[j] != 0):
				if (arr[i] > arr[j]):
					inv_count += 1
	print('Even: %d' % (size % 2 == 0))
	print('Inv_count: %d' % inv_count)
	if (size % 2 and inv_count % 2 == 0):
		return True

	#	row with 0, count from end
	row_with_pointer = 0
	for i in range(size):
		for j in range(size):
			if (desk[i][j] == 0):
				row_with_pointer = size - i	
	print('Row: %d' % row_with_pointer)
	if (size % 2 == 0):
		if ((row_with_pointer % 2 and inv_count % 2 == 0) or (row_with_pointer % 2 == 0 and inv_count % 2)):
			return True

	return False




def find_all_neighbours(tab, size):
	pos0 = None
	res =  []
	for i in range(size):
		for j in range(size):
			if tab[i * size + j] == 0:
				pos0 = i * size + j
				break
		if pos0 is not None:
			break
	if pos0 % size != 0:
		new = list(tab)
		new[pos0], new[pos0 - 1] = new[pos0 - 1], new[pos0]
		res.append(tuple(new))
	if pos0 % size != size - 1:
		new = list(tab)
		new[pos0], new[pos0 + 1] = new[pos0 + 1], new[pos0]
		res.append(tuple(new))
	if pos0 / size >= 1: 
		new = list(tab)
		new[pos0], new[pos0 - size] = new[pos0 - size], new[pos0]
		res.append(tuple(new))
	if pos0 / size < size - 1:
		new = list(tab)
		new[pos0], new[pos0 + size] = new[pos0 + size], new[pos0]
		res.append(tuple(new))
	return res


def isInList(self, new, liste):
	if new in liste:
		return True
	return False
# ====================*******=====================

# g ---> stends for global
g_success = False
g_desk_size = 3
# g_solution = create_solution_desk(g_desk_size)
# desk = create_desk(g_desk_size)

# print(g_solution)
solution = [
	[1,2,3],
	[8,0,4],
	[7,6,5]
]
desk = [
	[1,6,4],
	[5,0,3],
	[2,7,8]
]
# desk = [
# 	[5,6,7],
# 	[4,0,8],
# 	[3,2,1]
# ]
# desk = (4,2,3,5,0,6,8,1,7)
# desk = (5,6,7,4,0,8,3,2,1)
# solution = (1,2,3,8,0,4,7,6,5)

# desk = [
# 	[5,6,7],
# 	[4,0,8],
# 	[3,2,1]
# ]

tmp = Node(desk, solution, 0, False)
opened = {
	tmp.f: [tmp]
}
closed_desks = []

start = time.time()
q = 0

last_f = tmp.f
while len(opened):
	# curr = heapq.heappop(opened)
	curr = opened[last_f][0]
	q += 1
	if curr.desk == solution and curr.h == 0:
		print('Puzzle solved!')
		print(len(opened))
		print(len(closed_desks))
		print(time.time() - start)
		print(q)
		quit()

	closed_desks.append(curr.desk)
	
	if len(opened[curr.f]) == 1:
		del opened[curr.f]
	else:
		opened[curr.f].remove(curr)


	for node in curr.expand():

		if node.desk in closed_desks:
			continue
		elif node.f not in opened:
			opened[node.f] = [node]
		elif opened[node.f][0].f < node.f:
			# some thing wrong here
			opened[node.f].insert(0, node)

	print(opened)
	last_f = min(opened)

	# print('---------------------')
	# print(curr)
	# print('----------------------')
	# print(opened)
	# input()
