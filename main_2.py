import time
import random
import sys

from heapq import heappop, heappush
#from colorama import Fore, Style, Back
import node_funcs
import heuristics


# ==============| F U N C T I O N S |==============

def print_desk(desk):
	if not isinstance(desk, list):
		print('This is not a list!')
		return False
	print()
	for row in desk:
		for col in row:
			print(col, end=" ")
		print()
	print()



def create_solution_desk(size):
	desk = [[0 for x in range(size)] for x in range(size)]
	sequence = list(range(size**2 - 1, 0, -1))
	sequence.insert(0, 0)
	closed_pointers = []
	pointers = {'i': 0, 'j': 0}

	#	1 - right; 2 - down; 3 - left; 4 - up
	dir = 0
	while sequence:

		dir += 1
		if dir > 4:
			dir = 1

		if dir == 1:
			for j in range(pointers['j'], size):
				if {'i': pointers['i'], 'j': j} not in closed_pointers:
					pointers['j'] = j
					closed_pointers.append(dict(pointers))
					desk[pointers['i']][j] = sequence.pop()

		elif dir == 2:
			for i in range(pointers['i'], size):
				if  {'i': i, 'j': pointers['j']} not in closed_pointers:
					pointers['i'] = i
					closed_pointers.append(dict(pointers))
					desk[i][pointers['j']] = sequence.pop()

		elif dir == 3:
			for j in range(size - 1, -1, -1):
				if  {'i': pointers['i'], 'j': j} not in closed_pointers:
					pointers['j'] = j
					closed_pointers.append(dict(pointers))
					desk[pointers['i']][j] = sequence.pop()

		elif dir == 4:
			for i in range(size - 1, -1, -1):
				if {'i': i, 'j': pointers['j']} not in closed_pointers:
					pointers['i'] = i
					closed_pointers.append(dict(pointers))
					desk[i][pointers['j']] = sequence.pop()
	return desk



def create_desk(size: int):
	candidates = list(range(0, size**2))
	desk = [[ 0 for x in range(size)] for x in range(size)]
	for i in range(size):
		for j in range(size):
			desk[i][j] = candidates.pop(random.randrange(0, len(candidates)))
	return desk



def select_from_opened(opened):
	if opened:
		curr = opened[0]
	else:
		return None

	size = len(opened)
	for i in range(size):
		if curr.f > opened[i].f:
			curr = opened[i]
	return curr



# def is_solvable(desk):
# 	size = len(desk)
# 	inv_count = 0
# 	arr = sum(desk, [])

# 	for i in range(len(arr)):
# 		for j in range(i + 1, len(arr)):
# 			if (arr[i] != 0 and arr[j] != 0):
# 				if (arr[i] > arr[j]):
# 					inv_count += 1
# 	print('Even: %d' % (size % 2 == 0))
# 	print('Inv_count: %d' % inv_count)
# 	if (size % 2 and inv_count % 2 == 0):
# 		return True

# 	#	row with 0, count from end
# 	row_with_pointer = 0
# 	for i in range(size):
# 		for j in range(size):
# 			if (desk[i][j] == 0):
# 				row_with_pointer = size - i
# 	print('Row: %d' % row_with_pointer)
# 	if (size % 2 == 0):
# 		if ((row_with_pointer % 2 and inv_count % 2 == 0) or (row_with_pointer % 2 == 0 and inv_count % 2)):
# 			return True

# 	return False

def print_all_parents(node):
	curr = node
	size = len(curr[3]) - 1
	res = ''
	while curr[4]:
		for i in range(size, -1, -1):
			for j in range(size, -1, -1):
				res = f'{str(curr[3][i][j])} {res}'
			res = '\n' + res
		res = '\n' + res
		curr = curr[4]
	print(res)


# ====================*******=====================

# g ---> stends for global
g_success = False
g_desk_size = 3

solution = create_solution_desk(3)
desk = [
	[1,6,4],
	[5,0,3],
	[2,7,8]
]
desk = [
	[5,6,7],
	[4,0,8],
	[3,2,1]
]




time_complexity = 0
# because we start with one node
space_complexity = 1
first_h = heuristics.euclidean(desk, solution)
# (f, h, g, desk, parent)
opened = [(first_h,first_h, 0, desk, False)]
# closed_desks = []
closed_desks = {}
timer_start = time.time()

while len(opened):
	curr = heappop(opened)
	if curr[3] == solution and curr[1] == 0:
		# print_all_parents(curr)
		q = 0
		that = curr[4]
		while that:
			q += 1
			that = that[4]
		# print(curr)
		print(q)
		print('Puzzle solved!')
		print(f'Time: {time.time() - timer_start}')
		print(f'Time complexity:	{time_complexity}')
		print(f'Space complexity:	{len(opened) + len(closed_desks)}')
		print(f'Space complexity2:	{space_complexity}')
		quit()

	# if curr[3] not in closed_desks:
	# 	closed_desks.append(curr[3])

	sas = []
	for i in range(3):
		for j in range(3):
			sas.append(curr[3][i][j])

	closed_desks[tuple(sas)] = curr[0]


	for desk in node_funcs.expand(curr[1], curr[3], solution):

		sas = []
		for i in range(3):
			for j in range(3):
				sas.append(desk[i][j])

		if tuple(sas) not in closed_desks:
			# h = heuristics.euclidean(desk, solution)
			h = heuristics.out_of_place(desk, solution)
			node = (h + curr[2] + 1, h, curr[2] + 1, desk, curr)
			heappush(opened, node)
			time_complexity += 1

		space_complexity += 1
